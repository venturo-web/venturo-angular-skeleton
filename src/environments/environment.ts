
export const environment = {
    production: false,
    apiURL: 'https://onboarding-api-2024.venturo.pro/api',
    baseUrl: 'http://127.0.0.1:4200/'
}
